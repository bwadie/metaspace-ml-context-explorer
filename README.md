# metaspace-ml-context-explorer
Shiny App to help users align and explore context-specific results reported in METASPACE-ML [paper](https://www.biorxiv.org/content/10.1101/2023.05.29.542736v2)
